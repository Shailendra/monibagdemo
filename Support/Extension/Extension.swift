//
//  Extension.swift
//  MoniBagDemo
//
//  Created by Shailendra on 12/01/22.
//
import Foundation
import UIKit

public extension String{
    
   static let kEmpty      = ""
   static let kComingSoon = "Coming Soon"
   static let kGoogleLink = "https://www.google.com"
   static let kSignIn     = "Login Successfully!"
   static let kSignUp     = "Registration Successfully!"
}

public extension UITableView{
    
    func refrashList(){
        return self.reloadData()
    }
}

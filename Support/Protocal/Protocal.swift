//
//  Protocal.swift
//  MoniBagDemo
//
//  Created by Shailendra on 12/01/22.
//
import Foundation


@objc protocol buttonDelegate {
    @objc optional func forgetPasswordAction()
    @objc optional func signupAction()
    @objc optional func loginAction()
    @objc optional func eyeAction()
    @objc optional func facebookAction()
    @objc optional func googleAction()
    @objc optional func tcAction()
}

//
//  OnBoardModel.swift
//  MoniBagDemo
//
//  Created by Shailendra on 13/01/22.
//

import Foundation

//===================================
//MARK:- OnBoardM
//===================================
class OnBoardM : NSObject{
    
    override init() {
    }
    
    deinit{}
}
//===================================
//MARK:- OnBoardCodable
//===================================
public struct OnBoardCodable : Codable {
    let title       : String?
    let description : String?
    let imageName   : String?
    
    enum CodingKeys: String, CodingKey {
        
        case title       = "title"
        case description = "description"
        case imageName   = "imageName"
    }
    
    public init(from decoder: Decoder) throws {
        let values       = try decoder.container(keyedBy: CodingKeys.self)
        self.title       = try values.decodeIfPresent(String.self, forKey: .title)       ?? .kEmpty
        self.description = try values.decodeIfPresent(String.self, forKey: .description) ?? .kEmpty
        self.imageName   = try values.decodeIfPresent(String.self, forKey: .imageName)   ?? .kEmpty
    }
    
}

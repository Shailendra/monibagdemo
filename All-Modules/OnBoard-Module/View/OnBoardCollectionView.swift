//
//  OnBoardCollectionView.swift
//  MoniBagDemo
//
//  Created by Shailendra on 13/01/22.
//

import Foundation
import UIKit

//===================================
//MARK:- UICollectionViewDelegate
//===================================
extension ViewController : UICollectionViewDelegate{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
}
//===================================
//MARK:- UICollectionViewDataSource
//===================================
extension ViewController :UICollectionViewDataSource{

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.onBoardData?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: OnBoardCollectionCell.identifier, for: indexPath) as! OnBoardCollectionCell
        if self.onBoardData != nil{
            cell.cellConfigure(onBoard:self.onBoardData![indexPath.item])
        }
        
        return cell
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let width = scrollView.frame.width
        self.currentPage = Int(scrollView.contentOffset.x/width)
        self.pageControl.currentPage = self.currentPage
    }
}

//=========================================
//MARK:- UICollectionViewDelegateFlowLayout
//=========================================
extension ViewController :UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height:  collectionView.frame.height)
    }
}


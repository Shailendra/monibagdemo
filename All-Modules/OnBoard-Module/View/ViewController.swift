//
//  ViewController.swift
//  MoniBagDemo
//
//  Created by Shailendra on 12/01/22.
//

import UIKit

class ViewController: BaseVC {

    //MARK:- Load View
    @IBOutlet weak var onBoardCollectionView : UICollectionView!
    var viewModel = OnBoardVM()
    var onBoardData : [OnBoardCodable]?
    var currentPage = 0
    @IBOutlet weak var pageControl : UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.onBoardData = self.viewModel.getSpashData()
        self.pageControl.numberOfPages = self.onBoardData?.count ?? 0
        self.pageControl.currentPage = self.currentPage
        self.initCollectionView()
    }
}

extension ViewController{
    
    internal func initCollectionView(){
        self.onBoardCollectionView.register(OnBoardCollectionCell.nib, forCellWithReuseIdentifier: OnBoardCollectionCell.identifier)
    }
}



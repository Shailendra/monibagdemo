//
//  OnBoardCollectionCell.swift
//  MoniBagDemo
//
//  Created by Shailendra on 13/01/22.
//

import UIKit

class OnBoardCollectionCell: UICollectionViewCell {

    //MARK:- PROPERTIES
    @IBOutlet weak var iconImageView    : UIImageView!
    @IBOutlet weak var titleLabel       : UILabel!
    @IBOutlet weak var descriptionLabel : UILabel!

    class var identifier : String { return String(describing: self)}
    class var nib : UINib {return UINib(nibName: identifier, bundle: nil)}
    
    //MARK:- Load View
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

extension OnBoardCollectionCell{
    
    public func cellConfigure(onBoard : OnBoardCodable){
        self.titleLabel.text       = onBoard.title       ?? .kEmpty
        self.descriptionLabel.text = onBoard.description ?? .kEmpty
        self.iconImageView.image   = UIImage(named: onBoard.imageName ?? .kEmpty)
    }
}

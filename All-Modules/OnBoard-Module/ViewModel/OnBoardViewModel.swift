//
//  OnBoardViewModel.swift
//  MoniBagDemo
//
//  Created by Shailendra on 13/01/22.
//

import Foundation


//===================================
//MARK:- OnBoardVM
//===================================
class OnBoardVM : NSObject{
    

}

extension OnBoardVM{
    
    public func getSpashData()->[OnBoardCodable]?{
        
        if let path = Bundle.main.path(forResource: "OnBoard", ofType: "json"){
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                return try! JSONDecoder().decode([OnBoardCodable].self, from: data)
            }catch{
                return nil
            }
        }
        return nil
    }
}


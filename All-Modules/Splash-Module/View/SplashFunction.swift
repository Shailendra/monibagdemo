//
//  SplashFunction.swift
//  MoniBagDemo
//
//  Created by Shailendra on 14/01/22.
//

import Foundation

extension SplashVC : buttonDelegate{
    
    @objc func signupAction() {
        self.MoveToAuthPage(isLoginPage: false)
        self.splashTableView.reloadData()
    }
    
    @objc func loginAction() {
        self.MoveToAuthPage(isLoginPage: true)
        self.splashTableView.reloadData()
    }
    
    @objc func forgetPasswordAction() {
        self.showAlert(message: .kComingSoon)
    }
    
    @objc func facebookAction() {
        self.showAlert(message: .kComingSoon)
    }
    
    @objc func googleAction() {
        self.showAlert(message: .kComingSoon)
    }
    @objc func tcAction() {
        self.MoveToTCPage()
    }
    
}

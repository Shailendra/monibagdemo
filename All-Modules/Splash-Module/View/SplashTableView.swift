//
//  SplashTableView.swift
//  MoniBagDemo
//
//  Created by Shailendra on 14/01/22.
//

import Foundation

import UIKit

extension SplashVC : UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}

extension SplashVC :UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: SplashLogoCell.identifier) as! SplashLogoCell
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: ButtonCell.identifier) as! ButtonCell
            cell.loginView.isHidden = false
            cell.signUpButton.addTarget(self, action: #selector(SplashVC.signupAction), for: .touchUpInside)
            cell.signInButton.addTarget(self, action: #selector(SplashVC.loginAction), for: .touchUpInside)
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: SocialCell.identifier) as! SocialCell
            cell.tcButton.isHidden = false
            cell.tcButton.addTarget(self, action: #selector(SplashVC.tcAction), for: .touchUpInside)
            cell.facebookButton.addTarget(self, action: #selector(SplashVC.facebookAction), for: .touchUpInside)
            cell.googleButton.addTarget(self, action: #selector(SplashVC.googleAction), for: .touchUpInside)
            return cell
        default:break
        }
        return UITableViewCell()
    }
}

//
//  SplashVC.swift
//  MoniBagDemo
//
//  Created by Shailendra on 13/01/22.
//

import UIKit

class SplashVC: BaseVC {

    //MARK:- PROPERTIES
    @IBOutlet weak var splashTableView : UITableView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initTableView()
    }
}

extension SplashVC{
    internal func initTableView(){
        self.splashTableView.register(SplashLogoCell.nib, forCellReuseIdentifier: SplashLogoCell.identifier)
        self.splashTableView.register(ButtonCell.nib, forCellReuseIdentifier: ButtonCell.identifier)
        self.splashTableView.register(SocialCell.nib, forCellReuseIdentifier: SocialCell.identifier)
    }
}

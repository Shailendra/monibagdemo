//
//  SplashLogoCell.swift
//  MoniBagDemo
//
//  Created by Shailendra on 14/01/22.
//

import UIKit

class SplashLogoCell: UITableViewCell {

    //MARK:- PROPERTIES
    
    class var identifier : String {  return String(describing: self)}
    class var nib : UINib { return UINib(nibName: identifier, bundle: nil) }
    
    //MARK:- Load View
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

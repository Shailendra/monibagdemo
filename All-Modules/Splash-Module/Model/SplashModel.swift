//
//  SplashModel.swift
//  MoniBagDemo
//
//  Created by Shailendra on 13/01/22.
//

import Foundation

class SpalshM : NSObject{
    
    override init() {
    }
    
    deinit{}
}

struct SpalshCodable : Codable {
    let welcome   : String?
    let imageName : String?
    let fast      : String?
    let send      : String?
    let join      : String?
    let tc        : String?
    let with      : String?
    let signup    : String?
    let signin    : String?
    
    enum CodingKeys: String, CodingKey {
        
        case welcome   = "Welcome"
        case imageName = "imageName"
        case fast      = "fast"
        case send      = "send"
        case join      = "join"
        case tc        = "tc"
        case with      = "with"
        case signup    = "signup"
        case signin    = "signin"
    }
    
    init(from decoder: Decoder) throws {
        let values     = try decoder.container(keyedBy: CodingKeys.self)
        self.welcome   = try values.decodeIfPresent(String.self, forKey: .welcome)   ?? .kEmpty
        self.imageName = try values.decodeIfPresent(String.self, forKey: .imageName) ?? .kEmpty
        self.fast      = try values.decodeIfPresent(String.self, forKey: .fast)      ?? .kEmpty
        self.send      = try values.decodeIfPresent(String.self, forKey: .send)      ?? .kEmpty
        self.join      = try values.decodeIfPresent(String.self, forKey: .join)      ?? .kEmpty
        self.tc        = try values.decodeIfPresent(String.self, forKey: .tc)        ?? .kEmpty
        self.with      = try values.decodeIfPresent(String.self, forKey: .with)      ?? .kEmpty
        self.signup    = try values.decodeIfPresent(String.self, forKey: .signup)    ?? .kEmpty
        self.signin    = try values.decodeIfPresent(String.self, forKey: .signin)    ?? .kEmpty
    }
    
}

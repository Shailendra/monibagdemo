//
//  SocialMediaCell.swift
//  MoniBagDemo
//
//  Created by Shailendra on 13/01/22.
//

import UIKit

class SocialCell: UITableViewCell {

    //MARK:- PROPERTIES
    @IBOutlet weak var googleButton    : UIButton!
    @IBOutlet weak var facebookButton  : UIButton!
    @IBOutlet weak var signupButton    : UIButton!
    @IBOutlet weak var loginTitleLabel : UILabel!
    @IBOutlet weak var tcButton        : UIButton!
    
    
    class var identifier : String { return String(describing: self)}
    class var nib : UINib { return UINib(nibName: identifier, bundle: nil)}
    
    //MARK:- Load View
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}

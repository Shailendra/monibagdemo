//
//  AuthVC.swift
//  MoniBagDemo
//
//  Created by Shailendra on 13/01/22.
//

import UIKit

class AuthVC: BaseVC {

    @IBOutlet weak var authTableView : UITableView!
    lazy var numberOfRows = 7
    lazy var isLoginPage = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initTableView()
    }
}

extension AuthVC{
    internal func initTableView(){
        self.authTableView.register(LogoCell.nib, forCellReuseIdentifier: LogoCell.identifier)
        self.authTableView.register(HeadCell.nib, forCellReuseIdentifier: HeadCell.identifier)
        self.authTableView.register(InputCell.nib, forCellReuseIdentifier: InputCell.identifier)
        self.authTableView.register(ForgetCell.nib, forCellReuseIdentifier: ForgetCell.identifier)
        self.authTableView.register(ButtonCell.nib, forCellReuseIdentifier: ButtonCell.identifier)
        self.authTableView.register(SocialCell.nib, forCellReuseIdentifier: SocialCell.identifier)
    }
}

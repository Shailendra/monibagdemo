//
//  ButtonCell.swift
//  MoniBagDemo
//
//  Created by Shailendra on 13/01/22.
//

import UIKit

class ButtonCell: UITableViewCell {

    //MARK:- PROPERTIES
    @IBOutlet weak var loginButton : UIButton!
    @IBOutlet weak var loginLabel  : UILabel!
    
    @IBOutlet weak var signUpButton : UIButton!
    @IBOutlet weak var signInButton : UIButton!
    @IBOutlet weak var loginView    : UIView!
   
    class var identifier : String { return String(describing: self)}
    class var nib : UINib { return UINib(nibName: identifier, bundle: nil)}
   
    //MARK:- Load View
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

//
//  LogoCell.swift
//  MoniBagDemo
//
//  Created by Shailendra on 13/01/22.
//

import UIKit

class LogoCell: UITableViewCell {

    //MARK:- PROPERTIES
    class var identifier : String {  return String(describing: self)}
    class var nib : UINib { return UINib(nibName: identifier, bundle: nil) }
    
    //MARK:- Load View
    override func awakeFromNib() {
        super.awakeFromNib()
    }    
}

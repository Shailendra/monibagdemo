//
//  SplashTableView.swift
//  MoniBagDemo
//
//  Created by Shailendra on 13/01/22.
//

import Foundation
import UIKit

extension AuthVC : UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}

extension AuthVC :UITableViewDataSource{
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: LogoCell.identifier) as! LogoCell
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: HeadCell.identifier) as! HeadCell
            cell.headingLabel.text = self.isLoginPage ? "Login" : "Sign Up"
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: InputCell.identifier) as! InputCell
            cell.inputTextField.placeholder = self.isLoginPage ? "Email ID or Username" : "Username"
            cell.iconImageView.image = UIImage(named: "ic-email")
            cell.inputTextField.delegate = self
            cell.inputTextField.keyboardType = .default
            cell.inputTextField.isSecureTextEntry = false
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: InputCell.identifier) as! InputCell
            cell.inputTextField.placeholder = self.isLoginPage ? "Password" : "Email ID"
            cell.iconImageView.image = UIImage(named: "ic-password")
            cell.inputTextField.delegate = self
            cell.inputTextField.keyboardType = .emailAddress
            cell.inputTextField.isSecureTextEntry =  self.isLoginPage ? true : false
            return cell
        case 4:
            if self.isLoginPage{
                let cell = tableView.dequeueReusableCell(withIdentifier: ForgetCell.identifier) as! ForgetCell
                cell.forgotButton.addTarget(self, action: #selector(AuthVC.forgetPasswordAction), for: .touchUpInside)
                return cell
            }
            else{
                let cell = tableView.dequeueReusableCell(withIdentifier: InputCell.identifier) as! InputCell
                cell.inputTextField.placeholder = "Password"
                cell.iconImageView.image = UIImage(named: "ic-password")
                cell.inputTextField.delegate = self
                cell.inputTextField.keyboardType = .default
                cell.inputTextField.isSecureTextEntry = true
                return cell
            }
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: ButtonCell.identifier) as! ButtonCell
            cell.loginLabel.text = self.isLoginPage ? "Login" : "Create"
            cell.loginLabel.isHidden = false
            cell.loginButton.isHidden = false
            cell.loginButton.addTarget(self, action: #selector(AuthVC.createAction), for: .touchUpInside)
            return cell
        case 6:
            let cell = tableView.dequeueReusableCell(withIdentifier: SocialCell.identifier) as! SocialCell
            cell.loginTitleLabel.isHidden = false
            cell.signupButton.isHidden = false
            cell.loginTitleLabel.text = self.isLoginPage ? "Don’t have an account?" : "Have any account?"
            cell.signupButton.setTitle(self.isLoginPage ? "Sign Up" : "Sign In", for: .normal)
            cell.facebookButton.addTarget(self, action: #selector(AuthVC.facebookAction), for: .touchUpInside)
            cell.googleButton.addTarget(self, action: #selector(AuthVC.googleAction), for: .touchUpInside)
           
            switch self.isLoginPage {
            case true:
                cell.signupButton.addTarget(self, action: #selector(AuthVC.signupAction), for: .touchUpInside)
            case false:
                cell.signupButton.addTarget(self, action: #selector(AuthVC.loginAction), for: .touchUpInside)
            }
            return cell
            
        default:break
        }
      return UITableViewCell()
    }
}

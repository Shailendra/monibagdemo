//
//  InputCell.swift
//  MoniBagDemo
//
//  Created by Shailendra on 13/01/22.
//

import UIKit
import SkyFloatingLabelTextField

class InputCell: UITableViewCell {

    //MARK:- PROPERTIES
    @IBOutlet weak var inputTextField : UITextField!
    @IBOutlet weak var iconImageView  : UIImageView!
    @IBOutlet weak var eyeButton      : UIButton!
    
    class var identifier : String { return String(describing: self)}
    class var nib : UINib { return UINib(nibName: identifier, bundle: nil)}
    
    //MARK:- Load View
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

//
//  ForgetCell.swift
//  MoniBagDemo
//
//  Created by Shailendra on 13/01/22.
//

import UIKit

class ForgetCell: UITableViewCell {

    //MARK:- PROPERTIES
    @IBOutlet weak var forgotButton : UIButton!
    
    class var identifier : String { return String(describing: self)}
    class var nib : UINib { return UINib(nibName: identifier, bundle: nil)}
    
    //MARK:- Load View
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}

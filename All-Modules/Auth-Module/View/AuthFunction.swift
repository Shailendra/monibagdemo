//
//  AuthFunction.swift
//  MoniBagDemo
//
//  Created by Shailendra on 14/01/22.
//

import Foundation


extension AuthVC : buttonDelegate{
   
    @objc func signupAction() {
        self.isLoginPage = false
        self.authTableView.reloadData()
    }
    
    @objc func loginAction() {
        self.isLoginPage = true
        self.authTableView.reloadData()
    }
    
    @objc func eyeAction() {
        self.authTableView.reloadData()
    }
    
    @objc func forgetPasswordAction() {
        self.showAlert(message: .kComingSoon)
    }
    
    @objc func createAction() {
        if self.isLoginPage {
            self.showAlert(message: .kSignIn)
        }
        else{
            self.showAlert(message: .kSignUp)
        }
    }
    @objc func facebookAction() {
        self.showAlert(message: .kComingSoon)
    }
    
    @objc func googleAction() {
        self.showAlert(message: .kComingSoon)
    }
}

//
//  BaseVC.swift
//  MoniBagDemo
//
//  Created by Shailendra on 12/01/22.
//

import UIKit

class BaseVC: UIViewController {

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    public func POP(animate:Bool = true){
        self.navigationController?.popViewController(animated: animate)
    }
    
    public func DISMISS(animate:Bool = true){
        self.dismiss(animated: animate, completion: nil)
    }
    
    public func showAlert(message: String = .kEmpty) {
        let alert = UIAlertController(title: .kEmpty, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    public func MoveToTCPage(){
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TCVC") as! TCVC
        vc.modalTransitionStyle = .flipHorizontal
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    public func MoveToAuthPage(isLoginPage : Bool ){
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AuthVC") as! AuthVC
        vc.isLoginPage = isLoginPage
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

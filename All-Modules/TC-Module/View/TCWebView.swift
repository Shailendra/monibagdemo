//
//  TCWebView.swift
//  MoniBagDemo
//
//  Created by Shailendra on 14/01/22.
//

import Foundation
import WebKit


extension TCVC: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Started to load")
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("Finished loading")
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
    }
}

extension TCVC{
    
    internal func loadWebView(){
        self.webView.navigationDelegate = self
        self.webView.load(request)
    }
    
    @IBAction func backAction(){
        self.DISMISS()
    }
}


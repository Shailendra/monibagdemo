//
//  TCVC.swift
//  MoniBagDemo
//
//  Created by Shailendra on 14/01/22.
//

import UIKit
import WebKit
class TCVC: BaseVC {

    @IBOutlet weak var webView : WKWebView!
    var request : URLRequest!
    var viewModel = TC_VM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = URL(string: self.viewModel.model.urlStr)
        self.request = URLRequest(url: url!)
        self.loadWebView()
    }
}
